'use strict';

const fetch = require('node-fetch');
const api_host = 'https://www.eventbriteapi.com/v3';
var api_method = '/events/search/';
var api_args = '?categories=102&location.address=Jakarta';
var api_token = '&token=' + process.env.API_KEY_EVENTBRITE;

exports = module.exports = main;

function main(bot) {
    let keyword = ['/eventbrite', 'eventbrite', '/eventbrite@AlberstinBot', 'eventbrite@AlberstinBot'];
    bot.command(keyword, ctx => eventbrite(ctx));
}

function eventbrite(ctx) {
    let reply = '';
    fetch(api_host + api_method + api_args + api_token)
        .then(res => res.json())
        .then(json => {
            for (const eventb of json.events) {
                reply += `[${eventb.start.local.slice(0, 10)}] <a href="${eventb.url}">${eventb.name.text}</a>\n\n`
            }
            ctx.reply(reply, { 'parse_mode': 'html' });
        }).catch(err => console.error(err));
}